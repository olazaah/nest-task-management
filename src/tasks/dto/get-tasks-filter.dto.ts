import { TaskStatus } from "../interfaces/tasks.interface";
import { IsIn, IsNotEmpty, IsOptional } from "class-validator";

export class GetTaskFilterDto {
    @IsOptional()
    @IsIn([TaskStatus.IN_PROGRESS, TaskStatus.OPEN, TaskStatus.DONE])
    status: TaskStatus

    @IsOptional()
    @IsNotEmpty()
    search: string
}