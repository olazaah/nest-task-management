import { NotFoundException } from "@nestjs/common";
import { User } from "src/auth/user.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateTaskDto } from "./dto/create-task.dto";
import { GetTaskFilterDto } from "./dto/get-tasks-filter.dto";
import { TaskStatus } from "./interfaces/tasks.interface";
import { Task } from "./tasks.entity";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>{

    async getTasks(filterDto: GetTaskFilterDto, user: User): Promise<Task[]> {
        const { status, search } = filterDto

        const query = this.createQueryBuilder('task')
        query.where('task.userId = :userId', {userId: user.id})

        if (status){
            query.andWhere('task.status = :status', { status })
        }

        if (search) {
            query.andWhere('task.title LIKE :search OR task.description LIKE :search', { search: `%${search}%` })
        }

        const tasks = await query.getMany()

        return tasks
    }

    async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
        const { title, description } = createTaskDto
        const task = new Task()
        task.title = title
        task.description = description
        task.status = TaskStatus.OPEN
        task.user = user

        await task.save()

        delete task.user

        return task
    }

    async deleteTask(id: number, user: User): Promise<void> {
        const result = await this.delete({id, userId: user.id })
        if (result.affected == 0){
            throw new NotFoundException(`Task with id -> ${id} not found`)
        }
    }

    async updateTaskStatus(task: Task, status: TaskStatus): Promise<Task> {
        task.status = status
        
        await task.save()

        return task
    }
}